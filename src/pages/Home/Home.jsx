import React, { useEffect, lazy, Suspense } from 'react';

import http from '../../config/http';
import DottedLineLoader from '../../components/common/Loaders/DottedLineLoader';
const RegisteredEvents = lazy(() =>
  import('../../components/RegisteredEvents')
);

const Home = () => {
  useEffect(() => {
    http.get('/test').then(res => console.log(res));
  });
  return (
    <div>
      <Suspense fallback={<DottedLineLoader />}>
        <RegisteredEvents />
      </Suspense>
    </div>
  );
};

export default Home;
