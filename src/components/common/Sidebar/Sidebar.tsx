import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import './Sidebar.scss';

const Sidebar = (props:any) => {
  const { history } = props;
  const isActive = (pathname:string) => history.location.pathname === pathname;
  
  return (
    <div className='sidebar' data-color='purple' data-background-color='black' data-image='./assets/img/sidebar-2.jpg'>
      <div className='logo'>
        <NavLink to='/' className='simple-text logo-normal'>
          Excel Accounts
        </NavLink>
      </div>
      <div className='sidebar-wrapper'>
        <ul className='nav'>
          <li className={`nav-item ${isActive('/') ? 'active' : ''}`}>
            <NavLink className='nav-link' to='/'>
              <i className='material-icons'>dashboard</i>
              <p>Dashboard</p>
            </NavLink>
          </li>
          <li className={`nav-item ${isActive('/profile') ? 'active' : ''}`}>
            <NavLink className='nav-link' to='/profile'>
              <i className='material-icons'>person</i>
              <p>Profile</p>
            </NavLink>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default withRouter(Sidebar);